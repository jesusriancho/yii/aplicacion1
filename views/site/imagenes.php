<?php

use yii\helpers\Html;
?>

<h2>
   Cuatro Imagenes
</h2>
<br>
<?= Html::img('@web/imgs/4_1.jpg', [ // ruta de la imagen
    // atributos del img
    'alt' => 'cuatro',
    'class' => 'rounded img-thumbnail col-sm-2', 
]) ?>

<br>
<?= Html::img('@web/imgs/flores.jpg', [ // ruta de la imagen
    // atributos del img
    'alt' => 'flores',
    'class' => 'rounded img-thumbnail col-sm-2', 
]) ?>

<br>


<br>
<?= Html::img('@web/imgs/gato.jpg', [ // ruta de la imagen
    // atributos del img
    'alt' => 'gato',
    'class' => 'rounded img-thumbnail col-sm-2', 
]) ?>

<br>


<?= Html::img('@web/imgs/eggs.jpg', [ // ruta de la imagen
    // atributos del img
    'alt' => 'huevos',
    'class' => 'rounded img-thumbnail col-sm-2', 
]) ?>




