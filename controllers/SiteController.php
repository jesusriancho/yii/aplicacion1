<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

   
        

    public function actionPrueba()
    {
        return $this->render('prueba', [
            'valor' => 'aqui estoy'
        ]);
    }

    public function actionMensaje(){
        return $this->render('mostrarMensaje');
    }
    public function actionImagen(){
        return $this->render('imagen');
    }

    public function actionDias(){
        return $this->render('diasSemana');
    }

    public function actionMeses(){
        return $this->render('meses');
    }

    public function actionImagenes(){
        return $this->render('imagenes');
    }

    public function actionAccion1()
    {
        // recoge los datos del modelo o de un formulario
        // los trata
        // devolverle los resultados y/o datos a la vista

        $numero = 1;
        $numero1 = 2;
        $suma = $numero + $numero1;

        $datos = [1, 3, 5, 7, 9];
        $mayor = max($datos);

        $texto = "Ejemplo de clase";
        $e = substr_count(strtolower($texto), "e");
        return $this->render("accion1", [
            'numeros' => [$numero, $numero1],
            'suma' => $suma,
            'datos' => $datos,
            'mayor' => $mayor,
            'texto' => $texto,
            'ne' => $e
        ]);
    }

}
